import json
import tempfile

from kafka import KafkaConsumer
from minio import Minio

from analiseTextoNormas import TratamentoDasNormas
from conversor_LEXML_NBEX import CodexXMLtoDB


consumer = KafkaConsumer('codex-particula-normativa-lexml',
                         group_id='xxxtttxxyyx',
                         auto_offset_reset='earliest',
                         value_deserializer=lambda m: json.loads(m.decode('utf-8')))

minio_client = Minio("localhost:9000", "AKIAIOSFODNN7EXAMPLE", "wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY", secure=False)
tratamento_das_normas = TratamentoDasNormas()

for msg in consumer:
  value = msg.value
  arq_lexml = value['hashHtml'] + '.lexml'
  tmp_lexml = tempfile.gettempdir() + '/' + arq_lexml
  print (f"Processando arquivo {arq_lexml} - {tmp_lexml}")
  minio_client.fget_object(bucket_name='codex', file_path=tmp_lexml, object_name='lexml/temp-html' + arq_lexml)
  c = CodexXMLtoDB(tratamento_das_normas)
  c.fit_norma(filename=tmp_lexml)
  print('**JSON**')
  print(c.json)
  print('**Tuplas**')
  print(c.tuplas)
  print('**Tupls Nodes**')
  print(c.tuplas_nodes)
  print('----------')
