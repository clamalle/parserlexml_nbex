**Repositório de Estudos para o Conversor Lexml -> NBex (Codex)**

Trata, o presente repositório, de estudo para criação de conversor do formato lexml para o formato final (NBEX) de dados do projeto Codex, conforme consta no documento de Visão do Projeto.


---

## Arquivos presentes neste repositório



1. Arquivos .py - conversores que implementam as regras negociais.
2. amostras\textoMontado.js - neste arquivo há, unidos, uma sequencia de jsons - os quais estariam separados no banco de dados. Estes JSONs dizem respeito aos nós e arestas, implementam o documento denominado Arquitetura de dados.
3. decreto_9094_2017 - uma amostra de um arquivo lexml.


---

## Onde pode ser encontrada demais documentação sobre o projeto



1. Documento de Visão do Projeto - Este documento possui a visão geral bem como link para varios outros documentos. https://docs.google.com/document/d/1LNOD6eQMmBRm0fLyWjQxcdpDDrkxRq5WyTZlMe-ZXHM/edit?usp=sharing
2. Roadmap de desenvolvimento e implantação de tecnologias, Projeto Codex  - Vide link no final  do documento 1.
3. Documento de Arquitetura e Comportamento de Dados Codex - Vide link no final  do documento 1.
4. Proposta Conceitual Banco de Dados em Grafos, Projeto Codex - Vide link no final  do documento 1.
5. Fichas de Caso de Uso de Funcionalidades - Vide link no final  do documento 1.
6. Serviços Digitais à Sociedade Codex Descritivo  - Vide link no final  do documento 1.
7. Proposta de desenho de telas para a aplicação Portal de Normas - Vide link no final  do documento 1.


---
## Como testar funcionalidades

### Conversão .HTM (Legado Portal da Legislacao) para lexml 

a - um único diretório  

PASSO A PASSO, EXEMPLO:

    # importa o modulo de conversao HTM -> LEXML' 
    import conversorHTM_LEXML
    
    # instancia o modulo, neste exemplo decreto fica como padrao para as normas que nao  se conseguiu identificar seu tipo, 
    # lexml é o diretório que ira gravar os resultados do parser'
    teste_htm_to_lexml = conversorHTM_LEXML.CodexHTMtoDOCX('decreto', 'lexml')
    
    # Diretório com os arquivos .htm de normas que se queira parsear 
    # lembrando que o segundo parametro ( que tem por default False) se for ajustado para True toda vez que houver insucesso no parser
    # ele irá perguntar o que fazer ( entre forçar um parser_fake, abortar ou inserir manualmente os metadados) 
    teste_htm_to_lexml.fit_files("CCIVIL_03/_ato2015-2018/2017/decreto",  False)
    



b - todo o legado de uma única vez em loop
-- PREREQUISITOS : 
- um arquivo .csv de duas colunas, sendo a primeira em cada linha o path do diretorio onde estão contidos os .htm
- dependendicas do python e python3

PASSO A PASSO, EXEMPLO:

    # importa o modulo de conversao HTM -> LEXML' 
    import conversorHTM_LEXML
    
    # instancia o modulo, neste exemplo decreto fica como padrao para as normas que nao  se conseguiu identificar seu tipo, 
    # lexml é o diretório que ira gravar os resultados do parser'
    teste_htm_to_lexml = conversorHTM_LEXML.CodexHTMtoDOCX('decreto', 'lexml')
    
    # 'listaDiretorios.csv - arquivo com a lista dos diretorios a serem parseados, True  - se o script ira fazer perguntas ao 
    # usuario quando não conseguir parsear, em caso negativo ele vai direto ate o fim gerando os lexml e salvando os erros em .txt e     #os .docx das normas que nao conseguiu salvar. O ultimo parametro diz respeito ao local onde se encontra o repositorio de normas
    teste_htm_to_lexml.fit_directories_tree('listaDiretorios.csv', True, '/home/jef/Documentos/codex/CCIVIL_03')



 

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).
